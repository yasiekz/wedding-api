package services;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

public class Logger {

    public static java.util.logging.Logger getLogger(String className) {
        java.util.logging.Logger log = java.util.logging.Logger.getLogger(className);

        FileHandler fh = null;
        try {
            fh = new FileHandler("logs/wedding.log", true);
            log.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return log;
    }

}
