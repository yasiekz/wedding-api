import dagger.Module;
import dagger.Provides;
import org.mongodb.morphia.Datastore;
import orm.MorphiaProvider;
import services.Logger;

import javax.inject.Singleton;
import java.net.UnknownHostException;

@Module(
        injects = Main.class
)
class MainModule {
    @Provides
    @Singleton
    Datastore provideMorphiaORM() {
        Datastore db = null;
        MorphiaProvider morphiaProvider = new MorphiaProvider();
        try {
            db = morphiaProvider.getMorphiaOrm();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return db;
    }
}
