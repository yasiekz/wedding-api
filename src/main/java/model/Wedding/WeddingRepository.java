package model.Wedding;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import routes.exception.IllegalArgumentException;
import routes.exception.NotFoundException;

import javax.inject.Inject;
import java.util.List;

public class WeddingRepository {
    @Inject
    Datastore db;

    public List<Wedding> findAllPaginator(int page, int limit) {
        final Query<Wedding> query = db.createQuery(Wedding.class);
        return query.limit(limit).offset((page - 1) * limit).asList();
    }

    public Wedding findOne(String id) throws NotFoundException, IllegalArgumentException {
        if (!ObjectId.isValid(id)) {
            throw new IllegalArgumentException("Object id is invalid");
        }

        ObjectId objectId = new ObjectId(id);
        Wedding wedding = db.find(Wedding.class).field("_id").equal(objectId).get();
        if (wedding == null) {
            throw new NotFoundException("Object with ID = " + id + " not found");
        }

        return wedding;
    }

    public void save(Wedding wedding) {
        db.save(wedding);
    }

}
