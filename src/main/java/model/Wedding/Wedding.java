package model.Wedding;

import com.mongodb.BasicDBObject;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("weddings")
public class Wedding {
    @Id
    private ObjectId id;
    private int test;
    private String description;

    public Wedding(int test, String description) {
        this.test = test;
        this.description = description;
    }

    /** ORM (morphia) wymaga publicznego pustego konstruktora */
    public Wedding() {}

    public ObjectId getId() {
        return id;
    }

    public int getTest() {
        return test;
    }

    public String getDescription() {
        return description;
    }
}
