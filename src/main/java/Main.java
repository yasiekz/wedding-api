import dagger.ObjectGraph;
import routes.ExceptionsRoute;
import routes.WeddingRoute;

import javax.inject.Inject;

public class Main implements Runnable{
    @Inject
    WeddingRoute weddingRoute;
    ExceptionsRoute notFoundRoute = new ExceptionsRoute();

    public static void main(String[] args) {
        ObjectGraph objectGraph = ObjectGraph.create(new MainModule());
        Main main = objectGraph.get(Main.class);
        main.run();
    }

    @Override
    public void run() {
        System.out.print("started");
    }

}



