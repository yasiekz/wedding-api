package orm;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.net.UnknownHostException;

public class MorphiaProvider {

    public Datastore getMorphiaOrm() throws UnknownHostException {
        final Morphia morphia = new Morphia();

        morphia.mapPackage("model");
        MongoClient mongoClient;

        mongoClient = new MongoClient();

        Datastore datastore;
        datastore = morphia.createDatastore(mongoClient, "zstudio");
        datastore.ensureIndexes();

        return datastore;

    }
}
