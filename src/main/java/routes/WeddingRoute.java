package routes;

import dagger.Lazy;
import model.Wedding.WeddingRepository;
import services.JsonTransformer;

import javax.inject.Inject;
import java.util.logging.Logger;

import static spark.Spark.*;

public class WeddingRoute {
    @Inject
    Lazy<WeddingRepository> weddingRepository;

//    static Logger log = services.Logger.getLogger(WeddingRoute.class.getName());

    public WeddingRoute() {
        setupEndpoints();
    }

    private void setupEndpoints() {

        get("/get-weddings", (request, response) -> {

            response.header("Content-Type", "application/json");
            int randomNumber = 2 + (int) (Math.random() * 5);

            return weddingRepository.get().findAllPaginator(randomNumber, 1000);

        }, new JsonTransformer());

        get("/get-one/:id", (request, response) -> {
            return weddingRepository.get().findOne(request.params(":id"));
        }, new JsonTransformer());
    }

}
