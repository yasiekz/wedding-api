package routes;

import com.google.gson.Gson;
import routes.exception.IllegalArgumentException;
import routes.exception.NotFoundException;
import services.Logger;

import java.util.logging.Level;

import static spark.Spark.exception;

public class ExceptionsRoute {
    static java.util.logging.Logger logger = Logger.getLogger(ExceptionsRoute.class.getName());

    public ExceptionsRoute() {
        setupExceptions();
    }

    private void setupExceptions() {
        exception(NotFoundException.class, (e, request, response) -> {
            response.status(404);
            response.type("application/json");
            InnerException exception = new InnerException(e.getMessage(), 404);
            response.body(new Gson().toJson(exception));
        });

        exception(IllegalArgumentException.class, (e, request, response) -> {
            response.status(400);
            response.type("application/json");
            logger.log(Level.SEVERE, e.getMessage(), e);
            InnerException exception = new InnerException(e.getMessage(), 400);
            response.body(new Gson().toJson(exception));
        });
    }

    class InnerException {
        private String message;
        private int errorCode;

        public InnerException(String message, int errorCode) {
            this.message = message;
            this.errorCode = errorCode;
        }

        public InnerException(String message) {
            this.message = message;
            this.errorCode = 500;
        }
    }

}
